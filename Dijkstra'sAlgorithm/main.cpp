#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>

using namespace std;

int *algorithm(int **matrix, int v, int v1) {
    int *distances = new int[v];
    bool *visited = new bool[v];
    int index;

    for (int i = 0; i < v; ++i) {
        distances[i] = INT_MAX;
        visited[i] = false;
    }

    distances[v1] = 0;

    for (int i = 0; i < v - 1; ++i) {
        int min = INT_MAX;
        for (int j = 0; j < v; ++j) {
            if (!visited[j] && distances[j] <= min) {
                min = distances[j];
                index = j;
            }
        }

        visited[index] = true;

        for (int j = 0; j < v; ++j) {
            if (!visited[j] && matrix[index][j] && distances[index] != INT_MAX &&
                distances[index] + matrix[index][j] < distances[j]) {
                distances[j] = distances[index] + matrix[index][j];
            }
        }
    }
    return distances;
}

int main() {
    int numberOfV;
    cin >> numberOfV;
    int v1 = 0;
    int **matrix = new int *[numberOfV];

    for (int i = 0; i < numberOfV; ++i) {
        matrix[i] = new int[numberOfV];
    }

    for (int i = 0; i < numberOfV; ++i) {
        for (int j = 0; j < numberOfV; ++j) {
            matrix[i][j] = rand();
        }
    }

    int current = 0;

    for (int i = 0; i < 100; ++i) {
        srand(unsigned(time(0)));
        double start = clock();
        int *result = algorithm(matrix, numberOfV, v1);
        double end = clock();
        double time = end - start;
        current += time;
    }
    cout << current/100;
    delete matrix;
    return 0;
}

