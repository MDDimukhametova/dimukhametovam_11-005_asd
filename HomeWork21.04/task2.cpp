#include <iostream>

using namespace std;

bool contains(int **matrix, int m, int n, int x) {
    int left = 0;
    int right = m - 1;
    while (left <= right) {
        int mid = (left + right) / 2;
        if (matrix[mid][0] == x) {
            return true;
        } else if (matrix[mid][0] > x) {
            right = mid - 1;
        } else {
            left = mid + 1;
        }
    }

    int row = (left + right) / 2;
    left = 0;
    right = n - 1;
    while (left <= right) {
        int mid = (left + right) / 2;
        if (matrix[row][mid] == x) {
            return true;
        } else if (matrix[row][mid] > x) {
            right = mid - 1;
        } else {
            left = mid + 1;
        }
    }
    return false;
}

int main() {
    int m = 3;
    int n = 3;
    int **array = new int *[m];
    for (int i = 0; i < m; i++) {
        array[i] = new int[n];
    }

    for (int i = 0; i < m; ++i) {
        for (int j = 0; j < n; ++j) {
            cin >> array[i][j];
        }
    }

    int x;
    cin >> x;
    if (contains(array, m, n, x)) {
        cout << "true";
    } else cout << "false";

    return 0;
}