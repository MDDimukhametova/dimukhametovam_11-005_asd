#include <iostream>

using namespace std;

void findIndex(int *array, int arrayLength, int x) {
    int left1 = 0;
    int right1 = arrayLength;
    if (array[0] == x) right1 = 0;
    while (right1 - left1 > 1) {
        if (array[(left1 + right1) / 2] < x) {
            left1 = (left1 + right1) / 2;
        } else right1 = (left1 + right1) / 2;
    }

    int left2 = 0;
    int right2 = arrayLength;

    while (right2 - left2 > 1) {
        if (array[(left2 + right2) / 2] > x) {
            right2 = (left2 + right2) / 2;
        } else left2 = (left2 + right2) / 2;
    }

    if (right1 == left2) {
        cout << right1;
    } else {
        cout << "left " << right1 << endl;
        cout << "right " << left2 << endl;
    }

}

int main() {
    int arrayLength = 28;
    int x = 6;
    int *array = new int[arrayLength] {0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 2,2,
                                       2, 2, 3, 5, 5, 5, 5, 6, 9, 9, 9, 9, 9, 9};
    findIndex(array, arrayLength, x);
    return 0;
}