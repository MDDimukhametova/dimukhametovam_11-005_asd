#include <iostream>

using namespace std;

int sqrt(int x) {
    int left = 0;
    int right = x / 2;
    while (right - left > 1) {
        int mid = (right + left) / 2;
        if (mid*mid < x) {
            left = mid;
        } else if (mid*mid > x) {
            right = mid;
        } else return mid;
    }
    return right;
}

int main() {
    cout << sqrt(13);
    return 0;
}