#include <iostream>

using namespace std;

int median(int **matrix, int m, int n) {
    int min = -1;
    int max = -1;
    int minValue = INT_MAX;
    int maxValue = INT_MIN;
    for (int i = 0; i < m; i++) {
        if (matrix[i][0] < minValue) {
            min = i;
            minValue = matrix[i][0];
        }
        if (matrix[i][m - 1] > maxValue) {
            max = i;
            maxValue = matrix[i][m - 1];
        }
    }

    int left = matrix[min][0];
    int right = matrix[max][m - 1];

    while (right - left > 1) {
        int mid = (right + left) / 2;
        int diffMinus = 0;
        int diffPlus = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (matrix[i][j] < mid) diffMinus++;
                else if (matrix[i][j] > mid) diffPlus++;
            }
        }
        if (((diffMinus + diffPlus) % 2 > 0) && (abs(diffMinus - diffPlus) < 2)) {
            return mid;
        }
        if (diffPlus > diffMinus) left = mid;
        else if (diffMinus > diffPlus) right = mid;
        else if (diffPlus == diffMinus) return mid;
    }
}

int main() {
    int m = 3;
    int n = 3;
    int **matrix = new int *[m];
    for (int i = 0; i < m; ++i) {
        matrix[i] = new int[n];
    }
    for (int i = 0; i < m; ++i) {
        for (int j = 0; j < n; ++j) {
            cin >> matrix[i][j];
        }
    }
    cout << median(matrix, m, n);
}