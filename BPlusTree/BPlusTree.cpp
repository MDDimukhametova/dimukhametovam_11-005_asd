#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <ctime>

using namespace std;

struct Node {
    Node* parent;
    vector<int> keys;
    vector<int> pointers;
    vector<Node*> children;
    Node *left;
    Node *right;
    bool isLeaf;
};

struct BPlusTree {
    Node *root;
    int t;

    BPlusTree(int t) {
        this->root = NULL;
        this->t = t;
    }

    Node *findLeaf(int key) {
        Node *current = root;
        while (!current->isLeaf) {
            for (int i = 0; i < current->keys.size(); i++) {
                if (i == current->keys.size() || key < current->keys[i]) {
                    current = current->children[i];
                    break;
                }
            }
        }
        return current;
    }

    void insert(int key, int value) {
        Node *leaf = findLeaf(key);
        int position = 0;
        while (position < leaf->keys.size() && leaf->keys[position] < key) {
            position++;
        }
        for (int i = leaf->keys.size(); i > position; i--) {
            leaf->keys[i] = leaf->keys[i - 1];
            leaf->pointers[i] = leaf->pointers[i - 1];
        }
        leaf->keys[position] = key;
        leaf->pointers[position] = value;

        if (leaf->keys.size() == 2 * t) {
            split(leaf);
        }
    }

    void split(Node *node) {
        Node *newNode = new Node();
        newNode->right = node->right;
        node->right->left = newNode;
        node->right = newNode;
        newNode->left = node;

        int midKey = node->keys[t];
        for (int i = 0; i < newNode->keys.size(); i++) {
            newNode->keys[i] = node->keys[i + t + 1];
            newNode->pointers[i] = node->pointers[i + t + 1];
            newNode->children[i] = node->children[i + t + 1];
        }
        newNode->keys[0] = node->keys[t];
        newNode->pointers[0] = node->pointers[t];

        if (node == root) {
            root = newNode;
            root->keys[0] = midKey;
            root->children[0] = node;
            root->children[1] = newNode;
            node->parent = root;
            newNode->parent = root;
        } else {
            newNode->parent = node->parent;
            int position = 0;
            while (position < node->parent->keys.size() and node->parent->keys[position] < midKey) {
                position++;
            }
            for (int i = node->parent->keys.size(); i > position; i--) {
                node->parent->keys[i] = node->parent->keys[i - 1];
            }
            for (int i = node->parent->keys.size() + 1; i > position + 1; i--) {
                node->parent->children[i] = node->parent->children[i - 1];
                node->parent->keys[position] = midKey;
                node->parent->children[position + 1] = newNode;
            }
            if (node->parent->keys.size() == 2 * t) {
                split(node->parent);
            }
        }
    }

    void remove(int key) {
        Node *node = findLeaf(key);
        bool d = false;
        for (int i = 0; i < node->keys.size(); i++) {
            if (node->keys[i] == key) {
                d = true;
            }
        }
        if (!d) return;

        int position = 0;
        while (position < node->keys.size() && node->keys[position] < key) {
            position++;
        }

        for (int i = position; i < node->keys.size(); i++) {
            node->keys[i] = node->keys[i + 1];
            node->pointers[i] = node->pointers[i + 1];
        }

        for (int i = position + 1; i < node->keys.size() + 1; i++) {
            node->children[i] = node->children[i + 1];
        }

        Node *leaf = findLeaf(node->keys[position]);

        if (leaf->keys.size() < t - 1) {
            if (node->left != NULL && node->left->keys.size()) {
                for (int i = 1; i < node->keys.size(); i++) {
                    node->keys[i] = node->keys[i - 1];
                    node->pointers[i] = node->pointers[i - 1];
                    node->children[i] = node->children[i - 1];
                }
                node->children[node->keys.size()] = node->children[node->keys.size() - 1];
                node->keys[0] = node->left->keys[node->left->keys.size()];
                node->pointers[0] = node->left->pointers[node->left->keys.size()];
                node->children[0] = node->left->children[node->left->keys.size() + 1];

                update(node);
            } else if (node->right && node->right->keys.size() > t-1) {
                node->keys[node->keys.size() - 1] = node->right->keys[0];
                node->pointers[node->keys.size() - 1] = node->right->pointers[0];
                node->children[node->keys.size() - 1] = node->right->children[0];

                update(node);
            } else {
                if (node->left) {
                    for (int i = 0; i < node->keys.size(); i++) {
                        node->left->keys[node->left->keys.size()] = node->keys[i];
                        node->left->pointers[node->left->keys.size()] = node->pointers[i];
                        node->left->children[node->left->keys.size()] = node->children[i];

                        node->left->right = node->right;
                        node->right->left = node->left;

                        update(node->left);

                        remove(minKey(node));
                    }
                } else {
                    for (int i = 0; i < node->keys.size(); i++) {
                        node->keys[node->keys.size()] = node->right->keys[i];
                        node->pointers[node->keys.size()] = node->right->pointers[i];
                        node->children[node->keys.size() + 1] = node->right->children[i];
                        node->children[node->keys.size() + 2] = node->right->children[node->right->keys.size()];

                        node->right->left = node;
                        node->right = node->right->right;

                        update(node);

                        remove(node->parent, minKey(node->right));

                    }
                }
            }
            if (root->keys.size() == 1) {
                root = root->children[0];
            }
        }
    }

    void update(Node* node) {

    }

    int minKey(Node *node) {

    }
};

int main() {
    ifstream fin;
    fin.open("data/nums.txt");

    BPlusTree *tree = new BPlusTree(3);

    if (fin.is_open()) {
        int s;
        for (int i = 0; i < 4000000; ++i) {
            fin >> s;
            tree->insert(i, s);
        }
    }
    cout << 1;
    int s;
    cin >> s;

    double startTime = clock();
    tree->remove(344);
    double endTime = clock();
    double totalTime = endTime - startTime;


    fin.close();
    cout << endl;
    cout << totalTime;

    return 0;
}
