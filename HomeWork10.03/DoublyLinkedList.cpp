#include <iostream>

using namespace std;

struct Node {
    Node *prev;
    int item;
    Node *next;

    Node(Node *prev, int item, Node *next) {
        this->prev = prev;
        this->item = item;
        this->next = next;
    }
};

struct DoubleLinkedList {
    Node *head;

    DoubleLinkedList() {
        this->head = nullptr;
    }

    void add(int item) {
        if (head == nullptr) {
            head = new Node(nullptr, item, nullptr);
        } else {
            Node *current = head;
            while (current->next != nullptr) {
                current = current->next;
            }
            Node *newNode = new Node(current, item, nullptr);
            current->next = newNode;
        }
    }

    int size() {
        if (head == nullptr) {
            return 0;
        } else {
            int counter = 1;
            Node *current = head;
            while (current->next != nullptr) {
                counter++;
                current = current->next;
            }
            return counter;
        }
    }

    int get(int id) {
        Node *current = head;
        int i = 0;
        while (i != id) {
            current  = current->next;
            i++;
        }
        return current->item;
    }

    void insertAt(int id, int item) {
        if (head == nullptr) {
            head->item = item;
        } else {
            Node *current = head;
            int i = 0;
            while (i != id - 1) {
                current = current->next;
                i++;
            }
            Node *n = new Node(current, item, current->next);
            current->next = n;
        }
    }

    void removeAt (int id) {
        if (head == nullptr) {
            cout << "List is empty!" << endl;
            throw 1;
        } else {
            Node *current = head;
            int i = 0;

            while (i != id) {
                current = current->next;
                i++;
            }

            if (id == 0) {
                Node *t = head;
                head = head->next;
                delete t;
            } else if (current->next == nullptr) {
                Node *t = current;
                Node *p = current->prev;
                p->next = nullptr;
                delete t;
            } else {
                Node *t = current;
                current->prev->next = current->next;
                current->next->prev = current->prev;
                delete t;
            }
        }
    }

    void printAll() {
        if (head == nullptr) {
            cout << "List is empty!" << endl;
        } else {
            Node *t = head;
            while (t != nullptr) {
                cout << t->item << endl;
                t = t->next;
            }
        }
    }
};

int main() {
    DoubleLinkedList *doubleLinkedList = new DoubleLinkedList();
    doubleLinkedList->add(1);
    doubleLinkedList->add(2);
    doubleLinkedList->add(3);
    doubleLinkedList->add(5);
    doubleLinkedList->add(6);
    cout << "-------------" << endl;
    doubleLinkedList->printAll();
    cout << "-------------" << endl;
    cout << doubleLinkedList->size() << endl;
    cout << "-------------" << endl;
    cout << doubleLinkedList->get(3) << endl;
    cout << "-------------" << endl;
    doubleLinkedList->insertAt(3, 4);
    doubleLinkedList->printAll();
    cout << "-------------" << endl;
    /////
    doubleLinkedList->removeAt(5);
    cout << "-------------" << endl;
    doubleLinkedList->printAll();
    cout << doubleLinkedList->size() << endl;
    return 0;
}