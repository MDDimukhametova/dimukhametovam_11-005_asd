#include <iostream>

using namespace std;

struct Node {
    Node *prev;
    int item;
    Node *next;

    Node(Node *prev, int item, Node *next) {
        this->prev = prev;
        this->item = item;
        this->next = next;
    }
};

struct Queue {
    Node *head;
    Node *end;


    Queue() {
        this->head = nullptr;
        this->end = nullptr;
    }

    void enqueue(int item) {
        if (head == nullptr) {
            Node *n = new Node(nullptr, item, nullptr);
            head = n;
            end = n;
        } else {
            Node *current = end;
            Node *n = new Node(end, item, nullptr);
            current->next = n;
            end = n;
        }
    }

    int dequeue () {
        if (head == nullptr) {
            cout << "List is Empty!" << endl;
            throw 1;
        } else {
            Node *current = end;
            int i = current->item;
            Node *t = current->prev;
            t->next = nullptr;
            end = t;
            delete current;
            return i;
        }
    }

    void printAll() {
        if (head == nullptr) {
            cout << "List is Empty!" << endl;
        } else {
            Node *current = head;
            while (current != end) {
                cout << current->item << endl;
                current = current->next;
            }
            cout << end->item <<endl;
        }
    }
};

int main() {
    Queue *queue = new Queue;
    queue->enqueue(1);
    queue->enqueue(2);
    queue->enqueue(3);
    queue->printAll();
    cout << "-------------" << endl;
    cout << queue->dequeue() << endl;
    cout << "-------------" << endl;
    queue->printAll();
    return 0;
}
