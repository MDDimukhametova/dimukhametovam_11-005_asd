#include <iostream>

using namespace std;

int numberOfPaths(int size){
    int a = 0;
    int b = 1;
    int current = 0;
    for (int i = 0; i < size; ++i) {
        current = a + b;
        a = b;
        b = current;
    }
    return current;
}
int main() {
    int size;
    cin >> size;
    cout << numberOfPaths(size);
    return 0;
}
