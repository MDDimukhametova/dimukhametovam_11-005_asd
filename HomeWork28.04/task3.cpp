#include <iostream>

using namespace  std;

int numberOfPaths(int n, int m) {
    int **array = new int* [n];
    for (int i = 0; i < n; ++i) {
        array[i] = new int[n];
    }

    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            if (i == 0 || j == 0) {
                array[i][j] = 1;
            } else {
                array[i][j] = array[i - 1][j] + array[i][j - 1];
            }
        }
    }
    return array[n - 1][m - 1];
}

int main() {
    int n, m;
    cin >> n >> m;
    cout << numberOfPaths(n, m);
    return 0;
}
