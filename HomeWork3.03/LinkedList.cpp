#include <iostream>

using namespace std;

struct Node {
    int item;
    Node *next;

    Node(int item, Node *next) {
        this->item = item;
        this->next = next;
    }
};

struct LinkedList {
    Node *head;

    LinkedList(Node *node) {
        this->head = node;
    }

    void add(int item) {
        if (head == nullptr) {
            head = new Node(item, nullptr);
        } else {
            Node *current = head;
            while (current->next != nullptr) {
                current = current->next;
            }
            Node *newNode = new Node(item, nullptr);
            current->next = newNode;
        }

    }

    int size() {
        if (head == nullptr) {
            return 0;
        } else {
            int counter = 1;
            Node *current = head;
            while (current->next != nullptr) {
                counter++;
                current = current->next;
            }
            return counter;
        }
    }


    int get(int id) {
        Node *current = head;
        int i = 0;
        while (i != id) {
            current  = current->next;
            i++;
        }
        return current->item;
    }

    void printAll() {
        if (head == nullptr) {
            cout << "trouble" << endl;
        } else {
            Node *t = head;
            while (t != nullptr) {
                cout << t->item << endl;
                t = t->next;
            }
        }
    }
};

int main() {
    LinkedList *linkedList = new LinkedList(nullptr);
    linkedList->add(1);
    linkedList->add(2);
    linkedList->add(3);
    linkedList->printAll();
    cout << linkedList->size() << endl;
    int id;
    cin >> id;
    cout << linkedList->get(id) << endl;
    return 0;
}