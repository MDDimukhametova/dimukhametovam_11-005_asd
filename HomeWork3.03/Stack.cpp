#include <iostream>

using namespace std;

struct Node {
    int item;
    Node *next = nullptr;
};

struct Stack {
    Node *head = nullptr;

    void push(int item) {
        Node *node = new Node;
        node -> item = item;
        node -> next = head;
        head = node;
    }

    int pop() {
        if (head == nullptr) {
            cout << "Stack is empty!" << endl;
            throw 1;
        } else {
            Node *t = head;
            int x = t -> item;
            head = t -> next;
            delete t;
            return x;
        }
    }

    int peek() {
        if (head == nullptr) {
            cout << "Stack is empty!" << endl;
            throw 1;
        } else {
            return head ->item;
        }
    }

    void printAll() {
        if (head == nullptr) {
            cout << "Stack is empty!" << endl;
        } else {
            while (head != nullptr) {
                cout << pop() << endl;
            }
        }
    }
};


int main() {
    Stack *stack = new Stack();

    stack->push(1);
    stack->push(2);
    stack->push(3);
    stack->push(4);


    cout << stack->pop() << endl;

    cout << stack->peek() << endl;

    stack->printAll();
    return 0;
}
