#include <iostream>

using namespace std;

struct Node {
    int item;
    Node *next;

    Node(int item, Node *next) {
        this->item = item;
        this->next = next;
    }
};

struct SortedList {
    Node *head;

    SortedList() {
        head = nullptr;
    }

    void add(int item) {
        if (head == nullptr) {
            head = new Node(item, nullptr);
        } else if (head -> item >= item) {
            Node *node = new Node(item, head);
            Node *current = head;
            head = node;
            head -> next = current;
        } else {
            Node *current = head;
            while (current->next != nullptr && current->next->item <= item) {
                current = current->next;
            }
            if (current->next == nullptr) {
                Node *node = new Node(item, nullptr);
                current->next = node;
            } else {
                Node *node = new Node(item, current->next);
                current->next = node;
            }
        }
    }

    int get(int id) {
        Node *current = head;
        int count = 0;
        while (count != id) {
            current = current->next;
            count++;
        }
        return current->item;
    }

    int size() {
        int size = 0;
        if (head == nullptr) {
            return  size;
        } else {
            Node *current = head;
            while(current != nullptr) {
                size++;
                current = current->next;
            }
            return size;
        }
    }

    bool contains(int item) {
        Node *current = head;
        while (current != nullptr) {
            if (current->item == item) {
                return true;
            }
            current = current->next;
        }
        return false;
    }

    void removeId(int id) {
        if (id >= size() || id < 0) {
            cout << "trouble!" << endl;
        } else if (id == 0) {
            Node *current = head;
            head = head->next;
            delete current;
        } else {
            int pointer = 0;
            Node *current = head;
            while (pointer != id - 1) {
                current = current->next;
                pointer++;
            }
            Node *toDelete = current->next;
            current->next = current->next->next;
            delete toDelete;
        }
    }

    void printAll() {
        Node *current = head;
        while (current != nullptr) {
            cout << current->item << " ";
            current = current->next;
        }
    }
};

struct UniqueSortedList {
    Node *head;

    UniqueSortedList() {
        head = nullptr;
    }

    void add(int item) {
        if (head == nullptr){
            head = new Node(item, nullptr);
        } else {
            if (item <= head->item){
                if (item != head->item){
                    Node *node = new Node(item, head);
                    head = node;
                }
            } else {
                Node *current = head;
                while (current->next && current->next->item < item) {
                    current = current->next;
                }

                if (current->next == nullptr){
                    if(current->item != item) {
                        current->next = new Node(item, nullptr);
                    }
                } else {
                    if(current->next->item != item) {
                        Node *node = new Node(item, current->next);
                        current->next = node;
                    } else return;
                }
            }
        }
    }

    bool contains(int item) {
        Node *current = head;
        while (current != nullptr) {
            if (current->item == item) {
                return true;
            }
            current = current->next;
        }
        return false;
    }

    int get(int id) {
        Node *current = head;
        int count = 0;
        while (count != id) {
            current = current->next;
            count++;
        }
        return current->item;
    }

    int size() {
        int size = 0;
        if (head == nullptr) {
            return  size;
        } else {
            Node *current = head;
            while(current != nullptr) {
                size++;
                current = current->next;
            }
            return size;
        }
    }

    void removeId(int id) {
        if (id >= size() || id < 0) {
            cout << "trouble!" << endl;
        } else if (id == 0) {
            Node *current = head;
            head = head->next;
            delete current;
        } else {
            int pointer = 0;
            Node *current = head;
            while (pointer != id - 1) {
                current = current->next;
                pointer++;
            }
            Node *toDelete = current->next;
            current->next = current->next->next;
            delete toDelete;
        }
    }

    void printAll() {
        Node *current = head;
        while (current != nullptr) {
            cout << current->item << " ";
            current = current->next;
        }
    }
};

UniqueSortedList *Union(SortedList *a, SortedList *b) {
    UniqueSortedList *unionList = new UniqueSortedList();
    Node *current1 = a->head;
    Node *current2 = b->head;
    while (current1 != nullptr) {
        unionList->add(current1->item);
        current1 = current1->next;
    }
    while (current2 != nullptr) {
        unionList->add(current2->item);
        current2 = current2->next;
    }
    return unionList;
}

UniqueSortedList *Intersect(SortedList *a, SortedList *b) {
    UniqueSortedList *intersectList = new UniqueSortedList();

    for (int i = 0; i < a->size(); i++) {
        for (int j = 0; j < b->size(); j++) {
            if (a->get(i) == b->get(j)) {
                intersectList->add(a->get(i));
            }
        }
    }
    return intersectList;
}

UniqueSortedList *Difference(SortedList *a, SortedList *b) {
    UniqueSortedList *differenceList = new UniqueSortedList();

    for(int i = 0; i < a->size(); i++) {
        bool equal = false;
        Node *current = b->head->next;
        while(current->item != a->get(i)){
            if(current->next == nullptr){
                equal = true;
                break;
            }
            current = current->next;
        }
        if (equal) {
            differenceList->add(a->get(i));
            continue;
        }
    }
    return differenceList;
}

int main() {
    SortedList *sortedList = new SortedList();

    sortedList->add(5);
    sortedList->add(1);
    sortedList->add(3);
    sortedList->add(2);
    sortedList->add(3);

    cout << "sorted list: ";
    sortedList->printAll();

    cout << endl;
    cout << "3 element: " << sortedList->get(3) << endl;
    cout << "size of sorted list: " << sortedList->size() << endl;

    cout << "sorted list without 3 element: ";
    sortedList->removeId(3);
    sortedList->printAll();

    cout << endl;
    cout << "---------------" << endl;

    UniqueSortedList *uniqueSortedList = new UniqueSortedList();
    uniqueSortedList->add(9);
    uniqueSortedList->add(5);
    uniqueSortedList->add(7);
    uniqueSortedList->add(6);
    uniqueSortedList->add(7);

    cout << "unique sorted list: ";
    uniqueSortedList->printAll();

    cout << endl;
    cout << "---------------" << endl;

    SortedList *list1 = new SortedList();
    list1->add(7);
    list1->add(5);
    list1->add(3);
    list1->add(5);

    cout << "sorted list 1: ";
    list1->printAll();


    cout << endl;
    cout << "---------------" << endl;

    SortedList *list = new SortedList();
    list->add(9);
    list->add(7);
    list->add(5);
    list->add(7);
    list->add(10);

    cout << "sorted list 2: ";
    list->printAll();

    cout << endl;
    cout << "---------------" << endl;

    UniqueSortedList *unionList = Union(list1, list);

    cout << "union: ";
    unionList->printAll();

    cout << endl;
    cout << "---------------" << endl;

    UniqueSortedList *intersectList = Intersect(list1, list);

    cout << "intersect: ";
    intersectList->printAll();

    cout << endl;
    cout << "---------------" << endl;

    UniqueSortedList *differenceList = Difference(list, list1);

    cout << "Difference: ";
    differenceList->printAll();


    delete sortedList;
    delete uniqueSortedList;
    delete unionList;
    delete intersectList;
    delete differenceList;
    return 0;
}
